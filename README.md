# Related Blog Post Section for Shopify

Displays posts related to the current product based on the product metafield 'related_blog' and post metafield 'related_products'.

If no related posts are found, it will try to display posts based on the section settings.

If no section settings are provided, it will try to display posts from the default blog.

## Installation

1. Add the fallowing files to your theme:

-   `section-blog-posts.css` to `assets/` folder
-   `RelatedBlogPosts.liquid` to `sections/` folder
-   `RelatedPostCard.liquid` to `assets/` folder

2. Go to the theme customizer and add the section to the product page.

## Usage

1. Add a metafield to the product with the key `related_blog` and the value as the handle of the blog you want to display posts from.

![Product Metafieds Settings](images/product_metafields_definition.png)

![Product Metafield](images/product_metafields.png)

2. Add a metafield to the post with the key `related_products` and the value as the handle of the product you want to display the post on.

![Post Metafieds Settings](images/post_metafields_definition.png)

![Post Metafield](images/post_metafields.png)

Alternatively set the section settings to display posts from a specific blog.

![Post Metafield](images/section.png)

If nothing is set, the section will display posts from the default blog, but since shopify can have multiple blogs, it is recommended to set the section settings.

## Error Handling

If no articles found section will display error message in theme customizer.

### Additional Information

Styles are only for preview purposes, it may look different on your theme.
